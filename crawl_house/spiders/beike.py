#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2023/09/22 17:45:53
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   贝壳网
'''

import scrapy

class BeikeSpider(scrapy.Spider):
    name = 'beike'
    allowed_domains = ['cq.ke.com']
    start_urls = ['http://cq.ke.com/']

    def parse(self, response):
        pass
    def start_requests(self):
        url = self.base_url + 'xiaoqu/'
        yield scrapy.Request(url=url, callback=self.parse_district_links)

    def __init__(self):
        pass
