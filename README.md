# crawl_house

[![Version](https://img.shields.io/badge/version-v1.1.0-brightgreen)](https://git.yoqi.me/lyq/crawl_house)
[![.Python](https://img.shields.io/badge/Python-v3.8.5-brightgreen?style=plastic)](https://git.yoqi.me/lyq/crawl_house)

新房二手房租房爬虫,链家房源爬虫，通过小区信息爬取所有房源,基于scrapy

    
## 用法
    setting.py中配置MongoDB
    run run.py

## License

Licensed under the [Apache 2.0](LICENSE) © [liuyuqi.gov@msn.cn](https://github.com/jianboy)
